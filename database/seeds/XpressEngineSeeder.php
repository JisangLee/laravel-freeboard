<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class XpressEngineSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')
            ->insert(
               [
                   ['menu', '[]'],
                   ['page', '[]'],
                   ['comments', '[]'],
                   ['plugin', '[]'],
                   ['manage', '[]'],
                   ['board', '{"skinId":0,"perPage":10,"searchPerPage":10,"pageCount":10,"comment":true,"assent":true,"nonmember":false,"division":false,"revision":true,"functionOrder":0,"functionType":[],"editor":"PluginC\\Editor","multiUploader":"PluginC\\MultiUploader","useDocumentFileUploader":true,"useCommentFileUploader":true,"listColumns":["title","writer","createdAt","assentCount","dissentCount","readCount","updatedAt"],"formColumns":["title","content"],"dynamicFieldList":[]}'],
               ]
            );
    }

}
