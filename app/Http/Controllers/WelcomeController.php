<?php namespace App\Http\Controllers;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	//여기 인덱스 메소드
	public function index()
	{
		return view('welcome'); //뷰파일 생성해서 전달해줘라.
        //return view('안녕하세용!~3333');
        //또는 routes파일에서 할수있음.
	}

    public function profile()   //프로파일 메소드
    {
        return '사용자의 프로파일을 봅니다';
    }

}
