<?php namespace App\Http\Controllers;

class PageController extends Controller { //같은 Controllers에 들어있는 컨트롤러들 모두 Controller를 상속받는다

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function page($pageId){
        if($pageId=='test'){
               abort(404);
        }
        return view('layouts.page',[ 'pageName' => $pageId ]);
    }
//레이아웃에 페이지라는 뷰파일에게 인자 전달하기.페이지네임을 페이지 아이디값으로 전
//page.blade.php가보면 $pageName 페이지 이름이 가도록 하는것. 컨트롤러에게 받은 이름을 이렇게 전달해라.
}
