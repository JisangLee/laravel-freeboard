<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Post;
use Validator;
use Input;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    /**
     * public function index(){} post의 목록을 보여주는 역할을 수행 /post     -->GET
     * public function create(){} post의 작성하는 화면을 보여주는 역할을 수행  /post/create     -->GET
     * public function store(){} create()에서 작성한 내용을 실제 DB에 넣는 역할을 수행 /post   -->POST
     * public function show($id)(){} 목록에서 선택된 post를 출력하는 역할을 수행  /post/{postId}    -->GET
     * public function edit($id){} 목록에서 수정하기를 눌러 post를 수정하는 화면을 보여주는 역할을 수행  /post/{postId}/edit  -->GET
     * public function update($id){}  edit()에서 변경한 내용을 실제 DB에 update하는 역할 수행  /post/{postId}     -->PUTIPATCH
     * public function destroy($id){}  post를 삭제하는 역할 수행   /post/{postId}   -->DELETE
     */
	public function index()
	{
    //post컨트롤러의 index 메소드

        //페이지별로 가져옴. 5개씩..
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        //모든 포스트 내용을 가져오기
//        $posts = Post::all();
        //뷰에 넘기기.
        return view('post.index', compact('posts'));

//        return '목록을 확인하는 화면 입니다.';

    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('post.create');
//        return '생성을 위한 화면 입니다. ';
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validator = Validator::make($data = Input::all(), Post::$rules);

        if ($validator->fails())
        {
            //리다이렉트 > 다시 이전페이지로 돌아가라. 이때 validate에서 보여준 에러 같이처리.
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $post = new Post;
        if(Input::hasFile('thumbnail'))
        {
            $thumbnail = Input::file('thumbnail');

            $newFileName = time().'-'.$thumbnail->getClientOriginalName();
    //스토리지 패스
            $thumbnail->move(storage_path().'/files/', $newFileName);
            $post->thumbnail = $newFileName;
        }


        $post->title = Input::get('title');
        $post->body = Input::get('body');

        $post->save();


        return redirect()->route('post.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	    //id에 해당하는 post를 찾아서 post.show 뷰에 던져 출력
        $post = Post::findOrFail($id);

        return view('post.show', compact('post'));

//        return '내용을 볼 수 있는 화면입니다.';
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $post = Post::find($id);
        return view('post.edit', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $post = Post::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Post::$rules);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $post->update($data);
        return redirect()->route('post.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Post::destroy($id);
        return redirect()->route('post.index');
	}
//	public function page($pageId){
//	    return view('layouts.page',[ 'pageName' => $pageId ]);
//    }

}
