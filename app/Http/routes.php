<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// get, any, resource?? 이건http. 겟 포스트.. Method임. 뒤에 인자로
// 첫번째 인재로 들어가는것은 '/' 이건 resource
// 두번째 인자(function(){ return 'Hello World'}는 콜백.
Route::get('/', 'WelcomeController@index');

//Route::get('pages/aboutus','PageController@aboutus');
//Route::get('pages/location','PageController@location');
//Route::get('pages/copyright','PageController@copyright');
Route::get('/pages/{pageId}','PageController@page');
//Route::get('user/profile',['as'=>'profile',
//    'uses'=>'WelcomeController@profile']);
//Route::get('test',function(){
//
//    return route('profile');
//});
//Route::get('/',function(){
//    return 'Hello World!!!';
//});




//이걸 풀이하자면 사용자가 '/' URL로 접근하면 WelcomeController의 index 메소드를 실행하여 결과를 되돌려준다(@index)
//WelcomeController는 app/http/Controllers에 있음.
Route::resource('post', 'PostController');

/*
 * Named Route
 * 'as' 키워드를 사용하면 route에 이름을 지정 할 수 있다.
 * Route::get('user/profile',['as'=>'profile','uses' => 'UserController@showProfile']);
 * 이라우트는 profile이라는 이름을 지정해서 하겠다.
*/
