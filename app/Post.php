<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    public static $rules = [
        //앞으로 제목과 내용은 무조건 있어야함. 널 ㄴㄴ
//        'id' => 'required',  //id는 자동 inscreasement라 값을 입력하는게아니라 자동 데이터생성시 만들어짐. 여기다 이렇게 기술하면 에러남..
        'title' => 'required',
        'body' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = ['title', 'body', 'thumbnail'];

}
