@extends('layouts.master')
@section('content')
<h1>All Posts</h1>
  <ul class="list-group">
      <li class="list-group-item">

          <a>번호</a><a>제목</a>
      </li>
    @foreach($posts as $post)
      <li class="list-group-item">
          <a>{{$post->id}}</a>
          <a href="{{ route("post.show", $post->id) }}">{{ $post->title }}</a>

      </li>
    @endforeach
  </ul>

  {!! $posts->render() !!}

  <h3>
      <a href="{{ route('post.create') }}" class="btn btn-primary"> 글 작성하기 </a>
  </h3>
@stop
